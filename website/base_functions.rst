--------------------
Base Functions
--------------------

Reading the INP-File
--------------------

Functions to analyse the precipitations series based on the core method.

.. automodule:: swmm_api.input_file.inp_reader
    :members:
    :no-undoc-members:
